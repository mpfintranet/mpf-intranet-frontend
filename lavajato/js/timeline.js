var instancias = [];

$(document).ready(function(){
    
    $('.linha-item').each(function(){
        instancias = $(this).attr('data-instancias').split(',');
        if($('#instancia1').prop("checked") == true && instancias.includes("1")) {$(this).show();} 
        else if($('#instancia2').prop("checked") == true && instancias.includes("2")) {$(this).show();} 
        else if($('#instancia3').prop("checked") == true && instancias.includes("3")) {$(this).show();} 
        else if($('#instancia4').prop("checked") == true && instancias.includes("4")) {$(this).show();} 
        else if($('#instancia5').prop("checked") == true && instancias.includes("5")) {$(this).show();} 
        else if($('#instancia6').prop("checked") == true && instancias.includes("6")) {$(this).show();} 
        else if($('#instancia7').prop("checked") == true && instancias.includes("7")) {$(this).show();} 
        else {$(this).hide();}
    });
    
	var my_posts = $('[rel=tooltip]');
	var size = $(window).width();
	for(i=0;i<my_posts.length;i++){
		the_post = $(my_posts[i]);
		if(the_post.hasClass('invert') && size >=767 ){
			the_post.tooltip({ placement: 'left'});
			the_post.css('cursor','pointer');
		}else{
			the_post.tooltip({ placement: 'rigth'});
			the_post.css('cursor','pointer');
		}
	}
});

$('.form-check-input').change(function() {
    $('.linha-item').each(function(){
        instancias = $(this).attr('data-instancias').split(',');
        if($('#instancia1').prop("checked") == true && instancias.includes("1")) {$(this).show();} 
        else if($('#instancia2').prop("checked") == true && instancias.includes("2")) {$(this).show();} 
        else if($('#instancia3').prop("checked") == true && instancias.includes("3")) {$(this).show();} 
        else if($('#instancia4').prop("checked") == true && instancias.includes("4")) {$(this).show();} 
        else if($('#instancia5').prop("checked") == true && instancias.includes("5")) {$(this).show();} 
        else if($('#instancia6').prop("checked") == true && instancias.includes("6")) {$(this).show();} 
        else if($('#instancia7').prop("checked") == true && instancias.includes("7")) {$(this).show();} 
        else {$(this).hide();}
    });
});