/*eslint-env jquery*/

$(document).ready(function () {
    
    $('#megaMenu li a').each(function () {
        if($(this).parent().find('ul').length > 0) {
            $(this).prepend('<i class="fas fa-angle-right float-right d-none d-md-block"></i>');    
            $(this).prepend('<i class="fas fa-angle-down mr-2 d-block d-md-none float-left mt-1"></i>'); 
        }
    });
    $('#megaMenuRegiao li a').each(function () {
        if($(this).parent().find('ul').length > 0) {
            $(this).prepend('<i class="fas fa-angle-right float-right d-none d-md-block"></i>');
            $(this).prepend('<i class="fas fa-angle-down mr-2 d-block d-md-none float-left mt-1"></i>');
        }
    });

    toggleResponsive();

});

$(window).resize(function() {
    toggleResponsive();
});

function toggleResponsive() {
    if($(window).width() <= 767) {
        $('#megaMenu li a').each(function () {
            if($(this).parent().find('ul').length > 0) {
                $(this).addClass('hasSubmenu');
                $(this).parent().find('ul').find('a').addClass('ml-3');
                $(this).parent().find('ul li ul').find('a').addClass('ml-4');
                $(this).parent().find('ul li ul li ul').find('a').addClass('ml-5');
            }    
        });    
        $('.hasSubmenu').click(function () {
            $(this).next('ul').slideToggle("slow"); 
        });
    } else {        
        $('#megaMenu li a').each(function () {
            $(this).removeClass('hasSubmenu');
            $(this).parent().find('ul').find('a').removeClass('ml-3');
            $(this).parent().find('ul li ul').find('a').removeClass('ml-4');
            $(this).parent().find('ul li ul li ul').find('a').removeClass('ml-5');
        });       
        $('#megaMenu li ul').hide();
    }
}

$('#navMenu a').on('click', function (e) {
    if (!$(this).hasClass('hasSubmenu')) {
        if ($('#megaMenuRegiao').hasClass('open')) {
            $('#megaMenuRegiao').stop(true, true).slideUp('400');
            $('#megaMenuRegiao').toggleClass('open');
            $('#megaMenu').stop(true, true).slideDown('400');
            $('#megaMenu').toggleClass('open');
        } else if ($('#megaMenu').hasClass('open')) {
            $('#megaMenu').stop(true, true).slideUp('400');
            $('#megaMenu').toggleClass('open');
            $('.dark-screen').fadeTo(200, 0);
            $('.dark-screen').toggleClass('open');     
        } else {
            $('#megaMenu').stop(true, true).slideDown('400');
            $('#megaMenu').toggleClass('open');
            $('.dark-screen').fadeTo(200, 1);
            $('.dark-screen').toggleClass('open');
        }
    }
});

$('#navMenuRegiao a').on('click', function (e) {
    if (!$(e.target).is('select')) {
        if ($('#megaMenu').hasClass('open')) {
            $('#megaMenu').stop(true, true).slideUp('400');
            $('#megaMenu').toggleClass('open');
            $('#megaMenuRegiao').stop(true, true).slideDown('400');
            $('#megaMenuRegiao').toggleClass('open');
        } else if ($('#megaMenuRegiao').hasClass('open')) {
            $('#megaMenuRegiao').stop(true, true).slideUp('400');
            $('#megaMenuRegiao').toggleClass('open');
            $('.dark-screen').fadeTo(200, 0);
            $('.dark-screen').toggleClass('open');
        } else {
            $('#megaMenuRegiao').stop(true, true).slideDown('400');
            $('#megaMenuRegiao').toggleClass('open');
            $('.dark-screen').fadeTo(200, 1);
            $('.dark-screen').toggleClass('open');
        }
    }
});

$('.dark-screen').on('click', function () {
    if ($('#megaMenuRegiao').hasClass('open')) {
        $('#megaMenuRegiao').stop(true, true).slideUp('400');
        $('#megaMenuRegiao').toggleClass('open');
        $('.dark-screen').fadeTo(200, 0);
        $('.dark-screen').toggleClass('open');
    }

    if ($('#megaMenu').hasClass('open')) {
        $('#megaMenu').stop(true, true).slideUp('400');
        $('#megaMenu').toggleClass('open');
        $('.dark-screen').fadeTo(200, 0);
        $('.dark-screen').toggleClass('open');
    }
});

$('.megaMenuIndex li').mouseenter(function () {
    if($(window).width() > 767) {
        $('.megaMenuIndex li').removeClass('submenu-active');
        $('.megaMenuSub1 ul').html('<ul></ul>');
        $('.megaMenuSub2 ul').html('<ul></ul>');
        $('.megaMenuSub3 ul').html('<ul></ul>');
        if ($(this).find('ul').length > 0) {
            $(this).addClass('submenu-active');
            $('.megaMenuSub1 ul').html($(this).find('ul').html());
            $('.megaMenuSub1 li').mouseenter(function () {
                $('.megaMenuSub1 li').removeClass('submenu-active');
                $('.megaMenuSub2 ul').html('<ul></ul>');
                $('.megaMenuSub3 ul').html('<ul></ul>');
                if ($(this).find('ul').length > 0) {
                    $(this).addClass('submenu-active');
                    $('.megaMenuSub2 ul').html($(this).find('ul').html());
                    $('.megaMenuSub2 li').mouseenter(function () {
                        $('.megaMenuSub2 li').removeClass('submenu-active');
                        $('.megaMenuSub3 ul').html('<ul></ul>');
                        if ($(this).find('ul').length > 0) {
                            $(this).addClass('submenu-active');
                            $('.megaMenuSub3 ul').html($(this).find('ul').html());
                        }
                    });
                }
            });
        }
    }
});