$(document).ready(function () {
    
    if(!$('#sidebar').hasClass('active') && $(window).width <= 575) {
        $('#sidebar').toggleClass('active');
    }
    
    $('.sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
        $('#sidebar').toggleClass('open');
        $('#content').toggleClass('sidebarOpen');
        if($('#sidebar').hasClass('active')){
            $('.jsCalendar').addClass('active');
        } else {
            $('.jsCalendar').removeClass('active');
        }
    });  
    
    $('#content').on('click', function () {
        if($('#sidebar').hasClass('open')) {
            $('#sidebar').toggleClass('active');
            $('#sidebar').toggleClass('open');        
            $('#content').toggleClass('sidebarOpen');
        }
    });  
    
});