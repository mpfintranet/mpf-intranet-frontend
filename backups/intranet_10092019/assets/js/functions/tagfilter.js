$('.categoria-receita').click(function(){
    
    $('.categoria-receita').removeClass("active");
    $(this).addClass('active');
    
    var categoriaReceita = $(this).attr('id');
    
    $('.searchRECEITAS-item').each(function(){        
        $(this).removeClass("show");
        $(this).hide();
        if($(this).attr('data-categoria').indexOf(categoriaReceita) != -1) {
            $(this).addClass("show");
            $(this).show();
        }
    });
    
    if($('.searchRECEITAS-item.show').length <= 0)
        $('.searchRECEITAS-noresult').show();
    else 
        $('.searchRECEITAS-noresult').hide();
    
});

$('.tipo-receita').click(function(){
    
    $('.tipo-receita').removeClass("active");
    $(this).addClass('active');
    
    var tipoReceita = $(this).attr('id');
    
    $('.searchRECEITAS-item').each(function(){        
        $(this).removeClass("show");
        $(this).hide();
        if($(this).attr('data-tipo').indexOf(tipoReceita) != -1) {
            $(this).addClass("show");
            $(this).show();
        }
    });
    
    if($('.searchRECEITAS-item.show').length <= 0)
        $('.searchRECEITAS-noresult').show();
    else 
        $('.searchRECEITAS-noresult').hide();
    
});

$('.tag-todas').click(function(){
    
    $('.tipo-receita').removeClass("active");
    $('.categoria-receita').removeClass("active");
    
    $('.searchRECEITAS-item').each(function(){        
        $(this).addClass("show");
        $(this).show();
    });
    
});