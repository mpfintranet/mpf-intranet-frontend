//var contador = 0;
var delaySearchPIN = false;
var delaySearchRECEITAS = false;

$(document).ready(function(){    
    searchPIN();
    searchRECEITAS();
});

$('#searchPIN').keyup(function() {
    if(delaySearchPIN == false) {
        delaySearchPIN = true;
        setTimeout(function(){
            searchPIN();
            delaySearchPIN = false;
        }, 200);
    }
});
function searchPIN() {
    $('.searchPIN-item').each(function(){
        if($('#searchPIN').val() != "") {
            $(this).parent().parent().parent().parent().show();
            if($(this).text().toUpperCase().indexOf($('#searchPIN').val().toUpperCase()) != -1) {
                $(this).addClass("show");
                $(this).show();
            } else {
                $(this).removeClass("show");
                $(this).hide();
            }
        } else {            
            $(this).removeClass("show");
            $(this).hide();
            $(this).parent().parent().parent().parent().hide();
        }
    });
    
    if($('.searchPIN-item.show').length <= 0 && $('#searchPIN').val() != "")
        $('.searchPIN-noresult').show();
    else 
        $('.searchPIN-noresult').hide();
    
    //contador = $('.searchPIN-item.show').length;
    //$('#searchPIN-contador').text(contador);  
    
}

$('#searchRECEITAS').keyup(function() {
    if(delaySearchRECEITAS == false) {
        delaySearchRECEITAS = true;
        setTimeout(function(){
            searchRECEITAS();
            delaySearchRECEITAS = false;
        }, 200);
    }
});
function searchRECEITAS() {
    $('.searchRECEITAS-item').each(function(){
        if($('#searchRECEITAS').val() != "") {
            if($(this).text().toUpperCase().indexOf($('#searchRECEITAS').val().toUpperCase()) != -1) {
                $(this).addClass("show");
                $(this).show();
            } else {
                $(this).removeClass("show");
                $(this).hide();
            }
        } else {            
            $(this).addClass("show");
            $(this).show();
            
        }
    });
    
    if($('.searchRECEITAS-item.show').length <= 0 && $('#searchRECEITAS').val() != "")
        $('.searchRECEITAS-noresult').show();
    else 
        $('.searchRECEITAS-noresult').hide();
    
    //contador = $('.searchRECEITAS-item.show').length;
    //$('#searchRECEITAS-contador').text(contador);
    
}