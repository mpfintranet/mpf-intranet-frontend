$(document).ready(function () {
    $("*[data-includeHTML]").each(function () {                
        $(this).load($(this).attr("data-includeHTML"));
    });
});