$(document).ready(function () {
    
    $('.tag-noticiasLista').on('click', function () {
        $(this).toggleClass('active');
    });

    $('.list-group-item').on('click', function () {
        var listItens = document.getElementsByClassName("list-group-item");
        for (var i = 0; i < listItens.length; i++) {
            if (listItens[i].classList.contains('active')) {
                if (listItens[i] != $(this)) {
                    listItens[i].classList.remove('active')
                }
            }
        }
        $(this).toggleClass('active');
    });
    
    if($.cookie('giga-banner-close') == 'true') {
        $('#giga-banner').hide();
    }

});

$('#fecha-giga').click(function() { 
    $.cookie('giga-banner-close', 'true');
});

$(window).scroll(function () {
    $("a.ancora-conteudo").removeClass('ancora-hidden');

    if ($(this).scrollTop() > 50) {
        $("#navConteudos").addClass("fixo");
    } else {
        $("#navConteudos").removeClass("fixo");
        $("a.ancora-conteudo").addClass('ancora-hidden');
    }
});

$(".search").mouseenter(function () {
    $(this).addClass("open");
    $(".search-icon").addClass("open");
    $('.search-box').select();
});

$(".search-box").focusout(function () {
    $(".search").removeClass("open");
    $(".search-icon").removeClass("open");
});

function selectRegiao(regiao) {
    document.getElementById("noRegiao").innerHTML = regiao;
}

function invertButtons() {
    var emailBtn = document.getElementById("emailBtn");
    var compromissoBtn = document.getElementById("emailBtn");
    if (emailBtn.classList.contains('disabled')) {
        emailBtn.classList.remove('disabled')
        compromissoBtn.classList.add('disabled')
    } else {
        emailBtn.classList.add('disabled')
        compromissoBtn.classList.remove('disabled')
    }
}

function paginacaoActive(pagina) {
    var paginacao = document.getElementsByClassName("page-item");
    for (var i = 0; i < paginacao.length; i++) {
        paginacao[i].classList.remove("active");
    }
    pagina.classList.add("active");
}

function paginacaoNext() {
    var paginacao = document.getElementsByClassName("page-item");
    var index = -1;
    for (var i = 0; i < paginacao.length; i++) {
        if (paginacao[i].classList.contains("active")) {
            if (i + 1 > paginacao.length) {
                index = i;
            } else index = i + 1;
            paginacao[i].classList.remove("active");
        }
    }
    paginacao[index].classList.add("active");
}

function paginacaoReturn() {
    var paginacao = document.getElementsByClassName("page-item");
    var index = -1;
    for (var i = 0; i < paginacao.length; i++) {
        if (paginacao[i].classList.contains("active")) {
            if (i - 1 < 0) {
                index = i;
            } else index = i - 1;
            paginacao[i].classList.remove("active");
        }
    }
    paginacao[index].classList.add("active");
}

function paginacaoNextLast() {
    var paginacao = document.getElementsByClassName("page-item");
    for (var i = 0; i < paginacao.length; i++) {
        if (paginacao[i].classList.contains("active")) {
            paginacao[i].classList.remove("active");
        }
    }
    paginacao[paginacao.length - 1].classList.add("active");
}

function paginacaoReturnLast() {
    var paginacao = document.getElementsByClassName("page-item");
    for (var i = 0; i < paginacao.length; i++) {
        if (paginacao[i].classList.contains("active")) {
            paginacao[i].classList.remove("active");
        }
    }
    paginacao[0].classList.add("active");
}