function aumenta_ou_diminui_font(tipo, para_aumentar) {
    for (var i = 0; i < para_aumentar.length; i++) {
        para_aumentar[i].each(function (index, elem) {
            var font_size = $(elem).css('font-size').replace(/[a-z]/g, '');
            font_size = parseInt(font_size);
            if (tipo == "aumentar") {
                font_size += 1;
            } else {
                font_size -= 1;
            };
            font_size = String(font_size) + "px"
            $(elem).css('font-size', font_size);
        });
    }
}

/* ---------- Alto contraste do site ---------- */
$(".contraste").click(function (event) {
    $("html").toggleClass('alto-contraste');
    if ($.cookie('contraste') == "true") {
        $.cookie('contraste', 'false', {
            expires: 7,
            path: '/'
        });
    } else {
        $.cookie('contraste', 'true', {
            expires: 7,
            path: '/'
        });
    };
});
// Carrega dados gravados no cookie caso exista
if ($.cookie('contraste') == "true") {
    $("html").addClass('alto-contraste');
};


/*---------- Aumentar e diminuir fontes ---------- */
var para_aumentar = [$("h1"), $("h2"), $("h3"), $("h4"), $("h5"), $("p"), $("a"), $("li")];

if ($.cookie('font-size')) {
    var contador_font = parseInt($.cookie('font-size'));
} else {
    var contador_font = 0
};

$(".font-mais").click(function (event) {
    //Define um limite de aumento em ate 3x
    if (contador_font < 3) {
        contador_font += 1
        aumenta_ou_diminui_font("aumentar", para_aumentar);
        $.cookie('font-size', contador_font, {
            expires: 7,
            path: '/'
        })
    };
});
$(".font-menos").click(function (event) {
    //Define um limite de reducao
    if (contador_font > 0) {
        aumenta_ou_diminui_font("diminuir", para_aumentar);
        contador_font -= 1;
        $.cookie('font-size', contador_font, {
            expires: 7,
            path: '/'
        })
    };
});
$(".font-padrao").click(function (event) {
    $.removeCookie('font-size', {
        path: '/'
    });
    location.reload();    
});

// Carrega dados gravados no cookie caso exista
if ($.cookie('font-size') != 0) {
    var cont = $.cookie('font-size');
    while (cont > 0) {
        aumenta_ou_diminui_font("aumentar", para_aumentar);
        cont -= 1
    }
};